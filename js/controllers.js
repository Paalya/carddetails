angular.module('HcardApp.controllers', []).controller('Ctrl1', ['$scope','hcardFactory', function($scope, hcardFactory) {
	$scope.hcard = {};
    angular.copy(hcardFactory.getHcardDetails(), $scope.hcard);
	$scope.createHcard = function() {
		hcardFactory.setHcardDetails($scope.hcard);
	}
}])
.controller('Ctrl2', ['$scope','hcardFactory', function($scope, hcardFactory) {
	$scope.hcard = hcardFactory.getHcardDetails();
}]);

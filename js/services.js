angular.module('HcardApp.services', []).factory('hcardFactory', function() {
	var hCardDetails =  {
		personal: {
			name: '',
			surname: '',
			email: '',
			phone: ''
		},
		address: {
			house: '',
			street: '',
			suburb: '',
			state: '',
			postcode: '',
			country: ''
		}
	}

	return {
		getHcardDetails: function() {
			return hCardDetails;
		},
		setHcardDetails: function(obj) {
			angular.copy(obj, hCardDetails);
		}
	}
});	